# Descomplicando o Gitlab

Treinamento do BadTuxx - Descomplicando o Gitlab

Gravado no YouTube

### Day-1
```bash
 - Entendemos o que é o Git
 - Entendemos o que é o GitLab
 - Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git
 - Como criar uma branch
 - Como criar um merge request
 - Como criar um repositório Git
 - Como adicionar um membro no projeto
 - Como criar um grupo no GitLab
 - Como fazer o merge na master/main
```
